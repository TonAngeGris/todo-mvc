// Todo list
const list = document.getElementById('list');

// Add new todo block
const todoInput = document.getElementById('input-todo');
const todoAddBtn = document.getElementById('add-todo-btn');

// Option buttons
const checkAllBtn = document.getElementById('check-all-btn');
const deleteCompletedBtn = document.getElementById('delete-completed-btn');

// Filter buttons
const showAllBtn = document.getElementById('show-all-btn');
const showCompletedBtn = document.getElementById('show-completed-btn');
const showUncompletedBtn = document.getElementById('show-uncompleted-btn');

// Pagination
const pagination = document.getElementById('pagination-wrapper');

// Filter mode
let mode = 'all';
let lastMode = 'all';
// Current page
let lastPageAll = 0;
let curPage = 1;
// Number of todos per page
const pageOffset = 5;
// Key constant
const ENTER = 'Enter';
// Todo storage
let taskArray = [];

function render() {
  list.innerHTML = '';
  pagination.innerHTML = '';
  let paginationTaskArray = [];

  switch (mode) {
    case 'completed': {
      paginationTaskArray = taskArray.filter((el) => el.isCheck);
      break;
    }

    case 'uncompleted': {
      paginationTaskArray = taskArray.filter((el) => !el.isCheck);
      break;
    }

    default: {
      paginationTaskArray = taskArray;
      break;
    }
  }

  paginationTaskArray.forEach((el, i) => {
    if (pageOffset * (curPage - 1) < i + 1 && i + 1 <= pageOffset * curPage) {
      list.innerHTML += `
      <li class="task-block" data-id="${el.id}">
        <input type="checkbox" class="input-check-one" ${
          el.isCheck ? 'checked' : ''
        } /> 
        <input type="text" id="todo-text" value="${el.text}" readonly/>
        <button class="btn-delete-one-task">
          X
        </button>
      </li>
    `;
    }
  });

  for (
    let i = 1, j = 1;
    i < paginationTaskArray.length + 1;
    i += pageOffset, j++
  ) {
    pagination.innerHTML += `
      <button class="page-number ${
        j === curPage ? 'active' : ''
      }" id="page-number">${j}</button>
    `;
  }
}

function addTodo() {
  const todoText = todoInput.value;
  if (todoText !== '' && !todoText.match(/^\s+$/)) {
    const newTodo = {
      id: taskArray.length + 1,
      text: todoText,
      isCheck: false,
    };

    taskArray.push(newTodo);
    todoInput.value = '';
    curPage = Math.ceil(taskArray.length / pageOffset);
    render();
  }
}

function addTodoEnterHandler(event) {
  if (event.key === ENTER) {
    addTodo();
  }
}

function deleteTodo(event) {
  if (event.target.className !== 'btn-delete-one-task') return;

  const listElement = event.target.closest('.task-block');
  const todoId = Number(listElement.dataset.id);

  taskArray = taskArray.filter((el) => el.id !== todoId);
  console.log(taskArray);
  taskArray.forEach((el, i) => (el.id = i + 1));
  console.log(taskArray);
  render();
}

function checkTodo(event) {
  if (event.target.className !== 'input-check-one') return;

  const listElement = event.target.closest('.task-block');
  const todoId = Number(listElement.dataset.id);

  taskArray[todoId - 1].isCheck = !taskArray[todoId - 1].isCheck;
  render();
}

function checkAll() {
  taskArray.forEach((el) => (el.isCheck = true));
  render();
}

function deleteCompleted() {
  taskArray = taskArray.filter((el) => !el.isCheck);
  taskArray.forEach((el, i) => (el.id = i + 1));

  curPage = Math.ceil(taskArray.length / pageOffset);
  render();
}

function editTodo(event) {
  // event.target is input
  if (event.target.id !== 'todo-text') return;

  const textInput = event.target;

  textInput.readOnly = false;
  textInput.addEventListener('blur', addEditedTodo);
  textInput.addEventListener('keypress', addEditedTodoEnterHandler);
}

function addEditedTodo(event) {
  const textInput = event.target;

  if (textInput.value !== '' && !textInput.value.match(/^\s+$/)) {
    const newTodoText = textInput.value;
    const listElement = textInput.closest('.task-block');

    const todoId = Number(listElement.dataset.id);
    taskArray[todoId - 1].text = newTodoText;
    textInput.readOnly = true;
  }

  render();
}

function addEditedTodoEnterHandler(event) {
  if (event.key === ENTER) {
    addEditedTodo(event);
  }
}

function showAllTodos(event) {
  const btn = event.target;
  btn.classList.add('active');
  showCompletedBtn.classList.remove('active');
  showUncompletedBtn.classList.remove('active');
  mode = 'all';

  // Remember last page in "all" mode 
  // and avoid wrong page number in transition
  // from one function to another
  if (lastPageAll) {
    curPage = lastPageAll;
    lastPageAll = 0;
  }

  render();
  lastMode = mode;
}

function showCompletedTodos(event) {
  const btn = event.target;
  btn.classList.add('active');
  showAllBtn.classList.remove('active');
  showUncompletedBtn.classList.remove('active');
  mode = 'completed';

  // Remember last page in "all" mode 
  // and avoid wrong page number in transition
  // from one function to another
  if (!lastPageAll) {
    lastPageAll = curPage;
    curPage = 1;
  } else if (lastMode !== 'completed') {
    curPage = 1;
  }

  render();
  lastMode = mode;
}

function showUncompletedTodos(event) {
  const btn = event.target;
  btn.classList.add('active');
  showAllBtn.classList.remove('active');
  showCompletedBtn.classList.remove('active');
  mode = 'uncompleted';

  // Remember last page in "all" mode 
  // and avoid wrong page number in transition
  // from one function to another
  if (!lastPageAll) {
    lastPageAll = curPage;
    curPage = 1;
  } else if (lastMode !== 'uncompleted') {
    curPage = 1;
  }

  render();
  lastMode = mode;
}

function changePage(event) {
  if (event.target.id !== 'page-number') return;
  const pageBtn = event.target;
  console.log(pageBtn);
  const pageNumber = pageBtn.innerHTML;

  curPage = Number(pageNumber);
  console.log(curPage);

  render();
}

todoAddBtn.addEventListener('click', addTodo);
todoInput.addEventListener('keypress', addTodoEnterHandler);

checkAllBtn.addEventListener('click', checkAll);
deleteCompletedBtn.addEventListener('click', deleteCompleted);

list.addEventListener('click', deleteTodo);
list.addEventListener('click', checkTodo);
list.addEventListener('dblclick', editTodo);

showAllBtn.addEventListener('click', showAllTodos);
showCompletedBtn.addEventListener('click', showCompletedTodos);
showUncompletedBtn.addEventListener('click', showUncompletedTodos);

pagination.addEventListener('click', changePage);
